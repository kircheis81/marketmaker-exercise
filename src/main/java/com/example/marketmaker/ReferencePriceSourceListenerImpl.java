package com.example.marketmaker;

public class ReferencePriceSourceListenerImpl implements ReferencePriceSourceListener {

    private ReferencePriceSourceImpl source;

    public ReferencePriceSourceListenerImpl(ReferencePriceSourceImpl source) {
        this.source = source;
    }

    @Override
    public void referencePriceChanged(int securityId, double price) {
        source.set(securityId, price);
    }
}
