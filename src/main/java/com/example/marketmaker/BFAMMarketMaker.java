package com.example.marketmaker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class BFAMMarketMaker {
    private final int port;
    private boolean running;
    private final ExecutorService threadPool;
    private Thread mainThread;
    private QuoteCalculationEngine quoteCalculationEngine;
    private ReferencePriceSource referencePriceSource;

    public BFAMMarketMaker(int port, int numThreads, QuoteCalculationEngine calcEngine, ReferencePriceSource refPriceSource) {
        this.port = port;
        quoteCalculationEngine = calcEngine;
        referencePriceSource = refPriceSource;
        threadPool = Executors.newFixedThreadPool(numThreads, new ThreadFactory() {
            private final AtomicInteger threadCounter = new AtomicInteger();

            @Override
            public Thread newThread(Runnable r) {
                Thread t = Executors.defaultThreadFactory().newThread(r);
                t.setDaemon(true);
                t.setName("QUOTE_PROCESSOR_" + threadCounter.getAndIncrement());
                return t;
            }
        });
    }

    public void start() {
        running = true;
        mainThread = new Thread(() ->
        {
            try {
                ServerSocket server = new ServerSocket(port);
                while (running) {
                    final Socket client = server.accept();
                    threadPool.submit(() ->
                    {
                        try {
                            System.out.println(Thread.currentThread().getName() + ": Starting a quote thread for client @ " + client.getInetAddress().getHostAddress());
                            double quotePrice = -1.0;

                            try (BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()))) {
                                String line;
                                while ((line = in.readLine()) != null) {
                                    String[] quoteRequest = line.split(" ");
                                    if (quoteRequest.length == 3 && (quoteRequest[1].equalsIgnoreCase("BUY") || quoteRequest[1].equalsIgnoreCase("SELL"))) {
                                        try {
                                            int securityID = Integer.parseInt(quoteRequest[0]);
                                            boolean buy = quoteRequest[1].equalsIgnoreCase("BUY");
                                            int quantity = Integer.parseInt(quoteRequest[2]);
                                            double refPrice = referencePriceSource.get(securityID);
                                            quotePrice = quoteCalculationEngine.calculateQuotePrice(securityID, refPrice, buy, quantity);
                                        } catch (NumberFormatException ex) {
                                            System.out.println(Thread.currentThread().getName() + ":" + getClass().getName() + ": Quote request in wrong format: " + line);
                                        }
                                    } else {
                                        quotePrice = -1;
                                        System.out.println(Thread.currentThread().getName() + ":" + getClass().getName() + ": Quote request in wrong format: " + line);
                                    }

                                    // negative price means we dont want to offer. Let it timeout on client side
                                    if (quotePrice > 0) {
                                        PrintWriter out = new PrintWriter(client.getOutputStream(), true);
                                        System.out.println(Thread.currentThread().getName() + ":" + getClass().getName() + ": Quote responded with: " + quotePrice);
                                        out.println(quotePrice);
                                    }
                                }
                            }
                            client.close();
                        } catch (IOException ex) {
                            System.out.println(Thread.currentThread().getName() + getClass().getName() + ": Quote error on client");
                        }
                    });
                }
            } catch (IOException ex) {
                System.out.println("Error to start market maker");
            }
        });
        mainThread.setName("MARKETMARKER");
        mainThread.start();
    }

    public void stop() {
        running = false;
        if (mainThread != null) {
            mainThread.interrupt();
        }
        threadPool.shutdown();
        mainThread = null;
    }

    public static void main(String[] args) {
        // Just sample input here
        int port = 2017;
        int numThreads = 10;
        double spread = 0.001;
        double limit = 1000000000.0; // value limit for one position
        double reduceThreshold = 0.2;   // consider a quote to be reduce/increase risk if the position will be moved by 20%
        double reduceFactor = 0.8;
        double axeFactor = 0.7;
        long priceTimeout = 600000; // 10 min timeout

        ReferencePriceSourceImpl priceSource = new ReferencePriceSourceImpl(priceTimeout, true);
        QuoteCalculationEngineImpl quoteCalc = new QuoteCalculationEngineImpl(spread, limit, reduceThreshold, reduceFactor, axeFactor, true);
        BFAMMarketMaker marketmaker = new BFAMMarketMaker(port, numThreads, quoteCalc, priceSource);

        marketmaker.start();

        priceSource.set(123, 100.0);
        quoteCalc.updateAxe(123, true);
        quoteCalc.resetInventory(123);
        quoteCalc.updateInventory(123, 100000);


        priceSource.set(456, 200.0);
        quoteCalc.updateAxe(456, false);
        quoteCalc.resetInventory(456);
        quoteCalc.updateInventory(456, -100000);

        priceSource.set(789, 1.0);
        quoteCalc.resetInventory(789);
        quoteCalc.updateInventory(789, 999999999);

    }
}
