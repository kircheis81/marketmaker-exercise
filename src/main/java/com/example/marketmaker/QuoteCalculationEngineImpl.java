package com.example.marketmaker;

import java.util.concurrent.ConcurrentHashMap;

public class QuoteCalculationEngineImpl implements QuoteCalculationEngine {

    private double spread;
    private double limit;
    private double reduceFactor;
    private double reduceThreshold;
    private double axeFactor;
    private static final ConcurrentHashMap<Integer, Integer> inventory = new ConcurrentHashMap<Integer, Integer>();
    private static final ConcurrentHashMap<Integer, Boolean> axePosition = new ConcurrentHashMap<Integer, Boolean>();

    /**
     * Construct the engine
     * <p>
     *
     * @param spread          base bid offer spread
     * @param limit           absolute value limit for a single security position
     * @param reduceThreshold threshold to decide if the quote increase/reduce risk
     * @param reduceFactor    scale factor to adjust bid/offer spread, depends on if the quote increase/reduce risk
     * @param axeFactor       scale factor for axe position
     * @param randomInventory generate random inventory for testing
     */
    public QuoteCalculationEngineImpl(double spread, double limit, double reduceThreshold, double reduceFactor, double axeFactor, boolean randomInventory) {
        this.spread = spread;
        this.limit = limit;
        this.reduceThreshold = reduceThreshold;
        this.reduceFactor = reduceFactor;
        this.axeFactor = axeFactor;
        if (randomInventory) {
            for (int i = 1; i < 100000; i++)
                updateInventory(i, (int) Math.round((Math.random() - 0.5) * 100000));
        }
    }

    /**
     * Update the position inventory of a security
     * <p>
     *
     * @param securityId security identifier
     * @param quantity   new trade quantity
     */
    public void updateInventory(int securityId, int quantity) {
        inventory.compute(securityId, (k, v) -> (v == null) ? quantity : v + quantity);
    }

    /**
     * Reset the position inventory of a security
     * <p>
     *
     * @param securityId security identifier
     */
    public void resetInventory(int securityId) {
        inventory.put(securityId, 0);
    }

    /**
     * Update the axe position flag
     * <p>
     *
     * @param securityId security identifier
     * @param axe        if the position should be axe or not
     */
    public void updateAxe(int securityId, boolean axe) {
        axePosition.put(securityId, axe);
    }

    @Override
    public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
        System.out.println(Thread.currentThread().getName() + ":" + getClass().getName() +
                ": calculateQuotePrice(securityId, referencePrice, buy, quantity): " + securityId + "," + referencePrice + "," + buy + "," + quantity);

        int currPosition = inventory.getOrDefault(securityId, 0);
        System.out.println(Thread.currentThread().getName() + ":" + getClass().getName() + ": currPosition: " + currPosition);
        // client buy quote means BFAM sell
        int quotePosition = buy ? quantity * -1 : quantity;
        System.out.println(Thread.currentThread().getName() + ":" + getClass().getName() + ": quotePosition: " + quotePosition);

        // We cant trade over limit
        if (Math.abs(currPosition + quotePosition) * referencePrice >= limit) {
            System.out.println(Thread.currentThread().getName() + ":" + getClass().getName() + ": return -1 because over risk limit: " + limit);
            return -1;
        }

        double quoteSpread = spread;
        if (Math.abs(currPosition + quotePosition) <= Math.abs(currPosition * (1 - reduceThreshold))) {
            quoteSpread *= reduceFactor;
            System.out.println(Thread.currentThread().getName() + ":" + getClass().getName()
                    + " : base spread = " + spread + ": risk reduction quote: quoteSpread adjusted to " + quoteSpread);
        } else if (Math.abs(currPosition + quotePosition) >= Math.abs(currPosition * (1 + reduceThreshold))) {
            quoteSpread /= reduceFactor;
            System.out.println(Thread.currentThread().getName() + ":" + getClass().getName()
                    + " : base spread = " + spread + ": adding risk quote: quoteSpread adjusted to " + quoteSpread);
        }

        if (axePosition.getOrDefault(securityId, false)) {
            quoteSpread *= axeFactor;
            System.out.println(Thread.currentThread().getName() + ":" + getClass().getName() + ": axe position: quoteSpread adjusted to " + quoteSpread);
        }

        double spread = referencePrice * quoteSpread;
        System.out.println(Thread.currentThread().getName() + ":" + getClass().getName() + ": bid offer spread is " + spread);
        if (buy)
            return Math.round((referencePrice + spread / 2) * 10000.0) / 10000.0;
        else
            return Math.round((referencePrice - spread / 2) * 10000.0) / 10000.0;
    }
}
