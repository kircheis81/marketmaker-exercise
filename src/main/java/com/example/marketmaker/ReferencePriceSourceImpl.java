package com.example.marketmaker;

import java.util.concurrent.ConcurrentHashMap;

public class ReferencePriceSourceImpl implements ReferencePriceSource {
    /**
     * Reference Price with Timestamp. So we can ignore stale prices
     */
    private class TimestampedPrice {
        private double price;
        private long timestamp;

        public TimestampedPrice(double price) {
            this.price = price;
            this.timestamp = System.currentTimeMillis();
        }

        public double getPriceWithTimeout(long timeout) {
            if (timestamp + timeout >= System.currentTimeMillis())
                return price;
            else {
                System.out.println(Thread.currentThread().getName() + ":" + getClass().getName()
                        + " : the reference price is stale, return -1");
                return -1.0;
            }
        }
    }

    private static final ConcurrentHashMap<Integer, TimestampedPrice> referencePriceCache = new ConcurrentHashMap<Integer, TimestampedPrice>();
    private long timeout;

    /**
     * Construct the source
     *
     * @param timeout     timeout
     * @param randomPrice for test, if randomPrice (0-100) is generate (when no real price source exist)
     */
    public ReferencePriceSourceImpl(long timeout, boolean randomPrice) {
        this.timeout = timeout;
        if (randomPrice) {
            for (int i = 1; i < 100000; i++)
                set(i, Math.round(Math.random() * 100.0 * 10000.0) / 10000.0);
        }
    }

    @Override
    public void subscribe(ReferencePriceSourceListener listener) {
        // can be implemented with a proper price source api
    }

    @Override
    public double get(int securityId) {
        return referencePriceCache.getOrDefault(securityId, new TimestampedPrice(-1.0)).getPriceWithTimeout(timeout);
    }

    /**
     * Set the security price with timestamp
     *
     * @param securityId security identifier
     * @param price      security price
     */
    public void set(int securityId, double price) {
        referencePriceCache.put(securityId, new TimestampedPrice(price));
    }
}
