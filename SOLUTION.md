# Interview Solution

## Assumption

### Data sources

* `ReferencePriceSource` can get reference security mid price updates from one or multiple vendor feed. The updated
  price is added to the internal price cache, for QuoteCalculationEngine to read.
* `QuoteCalculationEngine` has two internal data sources inventory, axes positions. These two data sources can get
  update from other BFAM systems.

### Quote Response

* Client should have a timeout waiting for response. So if the MarketMaker doesn't send any response, client should
  consider it as "no interest to trade".
* Price has 4 decimal points.

## MarketMaker

A MarketMaker should have reference security prices, current inventory, axe positions, quote request, as well as model
input to decide the quote to client.

### Data

* Reference security price: current mid price on the market. It has a timestamp. So the algorithm can tell if a
  reference security price is stale, and thus can't be used anymore.The time period before a reference security price is
  consider stale is a parameter of the ReferencePriceSource.
* Current inventory of each security: it is the inventory that the MarketMaker holds, and gets updated once trade is
  done.
* Axe positions: trader decided list of positions for the MarketMaker to trade with lower bid/offer spread.
* Quote request: security id, buy/sell, and quantity.

### Model input

* Single security's value limit: the max absolute position value that the MarketMaker can accumulate. If a quote can
  result in a larger absolute position value, then the MarketMaker should ignore the quote request.
* Default bid/offer spread: the default bid/offer spread before any adjustment is applied.
* The threshold to decide if a quote reduce or increase risk after trade. For example 20% means absolute position change
  should be more than 20% to be considered as reduce or increase risk.
* The adjustment scale factor for quote that reduce or increase risk. Smaller bid/offer spread for quote that reduce
  risk, and larger otherwise.
* The adjustment scale factor for axe positions that trader thinks should be traded with smaller bid/offer spread.

### Design

The MarketMaker has a thread pool to handle multiple clients at the same time. Each client connecting to the MarketMaker
is handled by a separate quoting thread. All the quoting thread share the same QuoteCalculationEngine and
ReferencePriceSource.

### Future enhancement with external data caches

If the number of clients/requests can't be handled by this MarketMaker, we can have the following enhancement:

* All the data sources can be hosted out of the MarketMaker process, for example, TibcoRT/KDB/Gemfire etc. It also makes
  trouble-shooting easier because the data is easily accessible in the data storages.
* Have separate processes updating the reference price, inventory, axe position.
* Have a queue to store quote requests. So multiple MarketMaker can pickup the quote requests from there.

## Test

* MarketMaker: Run the Main() of BFAMMarketMaker with the Model input set for testing.
* Clients (multiple): telnet localhost 2017

### Model input

* Default bid/offer spread: 0.1%
* Single security's value limit: 1,000,000,000
* The threshold to decide if a quote reduce or increase risk after trade: 20%
* The adjustment scale factor for quote that reduce or increase risk: 0.8
* The adjustment scale factor for axe positions: 0.7
* Reference price is stale if 10 min old

### Case 1

Security 123, Inventory 100,000, is Axe, Reference price 100

Quote request: Position change after trade is less than 20%. Adjust bid/offer spread by 0.7

    123 BUY 19999

Quote response: 100.035 = 100 * (1 + 0.001 * 0.7 / 2)

    100.035

Quote request: Position to reduce by 20% if we do the trade with client. Adjust bid/offer spread by 0.7 and 0.8

    123 BUY 20000

Quote response: 100.028 = 100 * (1 + 0.001 * 0.7 * 0.8 / 2)

    100.028

Quote request: Position change after trade is less than 20%. Adjust bid/offer spread by 0.7

    123 SELL 19999

Quote response: 99.9563 = 100 * (1 - 0.001 * 0.7 / 2)

    99.965

Quote request: Position increase by 20% if we do the trade with client. Adjust bid/offer spread by 0.7 and 1/0.8

    123 SELL 20000

Quote response: 99.9563 = 100 * (1 - 0.001 * 0.7 / 0.8 / 2)

    99.9563

### Case 2

Security 456, Inventory -100,000, is not Axe, Reference price 200

Quote request: Position change after trade is less than 20%. Adjust bid/offer spread not adjusted

    456 BUY 19999

Quote response: 200.1 = 200 * (1 + 0.001 / 2)

    200.1

Quote request: Position increase by 20% if we do the trade with client. Adjust bid/offer spread by 1/0.8

    456 BUY 20000

Quote response: 200.125 = 200 * (1 + 0.001 / 0.8 / 2)

    200.125

Quote request: Position change after trade is less than 20%. Adjust bid/offer spread not adjusted

    456 SELL 19999

Quote response: 199.9 = 200 * (1 - 0.001 / 2)

    199.9

Quote request: Position reduce by 20% if we do the trade with client. Adjust bid/offer spread by 0.8

    456 SELL 20000

Quote response: 199.92 = 200 * (1 - 0.001 * 0.8 / 2)

    199.92

### Case 3

Security 789, Inventory 999,999,999, is not Axe, Reference price 1

Quote request: Position reduce by 20% if we do the trade with client. Adjust bid/offer spread by 0.8

    789 BUY 200000000

Quote response: 1.0004 = 1 * (1 + 0.001 * 0.8 / 2)

    1.0004

Quote request: Position change after trade is less than 20%. Adjust bid/offer spread not adjusted

    789 BUY 1

Quote response: 1.0005 = 1 * (1 + 0.001 / 2)

    1.0005

Quote request: Position value over limit if we do the trade with client.

    789 SELL 1

Quote response: no response because position value over limit. Log shows:

    return -1 because over risk limit: 1.0E9

### Case 4

10 min after starting the process, no response to the quote requests as the reference prices are all considered stale.
Log shows:

    the reference price is stale, return -1

### Case 5

Quote requests:

    123 buyy 100
    abc BUY 100
    123 BUY abc

Wrote format quote requests are ignored. Log shows:

    Quote request in wrong format